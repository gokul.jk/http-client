import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServiceService } from './service.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'httpclient';
  todo: any;
  // constructor(private http: ServiceService) {}
  // Event() {
  //   this.http
  //     .getTodoData('https://jsonplaceholder.typicode.com/todos/1')
  //     .subscribe((data) => {
  //       this.todo = data;
  //     });
  // }

  constructor(private http: HttpClient) {}
  Event() {
    this.http
      .get('https://sample-23f2f-default-rtdb.firebaseio.com/data.json')
      .subscribe((response: any) => {
        this.todo = response;
      });
  }
  onCreatePost(postData: { title: string; password: string }) {
    this.http
      .post(
        'https://sample-23f2f-default-rtdb.firebaseio.com/data.json',
        postData
      )
      .subscribe((response) => {
        console.log(response);
      });
  }
  delete() {
    this.http
      .delete('https://sample-23f2f-default-rtdb.firebaseio.com/data.json')
      .subscribe((response) => {
        console.log(response);
      });
  }
}
